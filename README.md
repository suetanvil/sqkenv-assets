# Assets for sqkenv

This repository contains various distributions of Squeak Smalltalk
(and related languages). It is used by
[sqkenv](https://codeberg.org/suetanvil/sqkenv).  All have been
released under open-source license(s).

These files are mostly (somewhat) large binaries and so are not
versioned in the traditional sense.  Instead, `sqkenv` uses git and
this repository to ensure that they arrive uncorrupted and
untampered-with.

Each Squeak distribution occupies the tip of its own branch with all
of the binaries added in a single commit and never modified; this
ensures that there are no unused large files trapped in the repo's
commit history.

In addition, each branch contains a shell script named `remake.sh`.
This is the script that was initially used to fetch files in the
branch tip. You can audit this file to confirm that it gets its files
from the official website, then run it and use `git` to confirm that
the contents are identical (assuming that the upstream site hasn't
changed anything).

